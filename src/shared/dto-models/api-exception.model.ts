export class ApiException {
    statusCode?: number;
    errorCode?: number;
    message?: string;
    status?: string;
    error?: string;
    errors?: any;
    timestamp?: string;
    path?: string;
}
