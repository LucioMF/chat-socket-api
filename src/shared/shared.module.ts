import { Module, Global } from '@nestjs/common';
import { ConfigurationService } from './configuration/configuration.service';
import { HttpErrorFilter } from './filters/http-error.filter';
import { LoggingInterceptor } from './interceptors/logging.interceptor';

@Global()
@Module({
  providers: [
    ConfigurationService,
    HttpErrorFilter,
    LoggingInterceptor,
  ],
  exports: [
    ConfigurationService,
    HttpErrorFilter,
    LoggingInterceptor,
  ],
  imports: [],
})
export class SharedModule { }
