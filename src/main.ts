import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { HttpErrorFilter } from './shared/filters/http-error.filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const hostDomain = AppModule.isDev ? `${AppModule.host}:${AppModule.port}` : AppModule.host;

  app.useGlobalFilters(new HttpErrorFilter());
  app.enableCors();
  await app.listen(AppModule.port);
}
bootstrap();
